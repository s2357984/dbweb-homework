<?php 
session_set_cookie_params(604800);
session_start();

try {
require_once('db-config.php');
$dbh = new PDO("mysql:dbname=$db;host=$host", $user, $pass);
}
catch (PDOException $e) {
echo 'Connection failed: ' . $e->getMessage();
exit;
}
//var_dump($_SESSION);
//error_reporting(-1);
//ini_set("display_errors", 1);

if (!isset($_SESSION['questionnr']))
{
$_SESSION['questionnr'] = 0;
$_SESSION['corAns'] = 0;
}
else
{
	if (!isset($_REQUEST['submit']))
	{
		$p=$dbh->prepare('select count(*) from choice where q_number=? AND correct=1 AND c_number=?;');
		$p->execute(array($_SESSION['questionnr'],$_REQUEST["points"]));
		if(current($p->fetch(PDO::FETCH_NUM)) == 1)
		{
		echo "You are correct!";
		$_SESSION['corAns'] += 1;
		}
		else
		{
		echo "You are wrong!";
		}
	}
	
	$x=$dbh->query('select count(*) from question;')->fetch();
	if($_SESSION['questionnr']+1 > $x[0])
	{
	echo "<br /><br />You are done!";
	echo "You have answered " . $_SESSION['corAns'] . " out of " . $x[0] . " correctly.";
	session_destroy();
	exit();
	}
	
}
$q=$dbh->prepare('select q_text from question where q_number=?;');
$q->execute(array($_SESSION['questionnr']+1));
$row = $q->fetch(PDO::FETCH_ASSOC);
$q_text = $row['q_text'];
$c=$dbh->prepare('select c_text,c_number from choice where q_number=?;');
$c->execute(array($_SESSION['questionnr']+1));
if (!isset($_REQUEST['submit']))
	{
	$_SESSION['questionnr']++;
	}
?><form action='index.php' method='GET'>
    <p><b><?php echo $q_text;?></b></p>
<?php while ($row = $c->fetch(PDO::FETCH_ASSOC))
{
echo '<input type="radio" name="points" value="' . $row['c_number'] . '">'
	. $row['c_text'] ;
}
?>
	<br>
	
	
    <br /> <input type="submit" name="submit" value="Submit" /><br />
	</form>