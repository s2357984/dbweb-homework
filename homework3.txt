1. The user can make changes in the URL. The number of points can simply be changed by altering the number after "points".
Also, it is possible to change the values in cookies.

2. With sessions it is not possible cheat, at least not in the way as in the previously answered question.
Sessions are server side and you can't change that.